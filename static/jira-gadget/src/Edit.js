import React, { useEffect, useState } from 'react';
import Form, { Field } from '@atlaskit/form';
import TextField from '@atlaskit/textfield';
import Button, { ButtonGroup } from '@atlaskit/button';
import { view, invoke } from '@forge/bridge';

function Edit() {
  const onSubmit = (formData) => view.submit(formData);

  return (
    <Form onSubmit={onSubmit}>
      {({ formProps, submitting }) => (
        <form {...formProps}>
          <Field name="boardId" label="Board Id">
            {({ fieldProps }) =>
            <TextField {...fieldProps} />}
          </Field>
          <br/>
          <ButtonGroup>
            <Button type="submit">Save</Button>
            <Button appearance="subtle" onClick={view.close}>Cancel</Button>
          </ButtonGroup>
        </form>
      )}
    </Form>
  );
}

export default Edit;
