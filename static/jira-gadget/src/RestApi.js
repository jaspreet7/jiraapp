import { requestJira } from '@forge/bridge';

const fetchAssigneeDataInSprint = async (boardId) => {
  const boardResponse = await requestJira('/rest/agile/1.0/board/' + boardId + '/sprint?state=active');
  const activeSprint = await boardResponse.json();
  const sprintResponse = await requestJira('/rest/agile/1.0/sprint/' + activeSprint.values[0].id + '/issue');
  const issueData = await sprintResponse.json();
  const assigneeMap = new Map();
  for(let issue of issueData.issues) {
    let assigneeName = issue.fields.assignee == null ? "Unassigned" : issue.fields.assignee.displayName;
    let status = issue.fields.status.name;
    let statusCategory = issue.fields.status.statusCategory.name;
    if(assigneeMap.has(assigneeName)) {
      const statusMap = assigneeMap.get(assigneeName);
      if(statusMap.has(status)) {
        statusMap.get(status).count += 1;
      } else {
        statusMap.set(status, {"category": statusCategory, "count": 1});
      }
      assigneeMap.set(assigneeName, statusMap);
    } else {
      assigneeMap.set(assigneeName, new Map().set(status, {"category": statusCategory, "count": 1}));
    }
  }
  return assigneeMap;
};

const fetchBoardColumn = async (boardId) => {
  const boardResponse = await requestJira('/rest/agile/1.0/board/' + boardId + '/configuration');
  const configuration = await boardResponse.json();
  const columnNameList = [];
  for(let column of configuration.columnConfig.columns) {
    columnNameList.push(column.name);
  }
  return columnNameList;
}

export {fetchAssigneeDataInSprint, fetchBoardColumn};