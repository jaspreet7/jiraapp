import React, { useEffect, useState } from 'react';
import { view, invoke } from '@forge/bridge';
import {Bar} from "react-chartjs-2";
import { BarElement,  CategoryScale,Chart as ChartJS,Legend, LinearScale,Title, Tooltip } from "chart.js";
import {fetchAssigneeDataInSprint, fetchBoardColumn} from './RestApi';

ChartJS.register(CategoryScale, LinearScale, BarElement,Title,Tooltip,Legend);

function View() {
  const [assigneeName, setAssigneeName] = useState([]);
  const [dataSet, setDataSet] = useState([]);
  const [boardId, setBoardId] = useState(0);

  useEffect(() => {
    view.getContext().then((con) => {
      let boardId = con.extension.gadgetConfiguration.boardId;
      setBoardId(boardId);
      fetchBoardColumn(boardId).then(function(columnNameList) {
        fetchAssigneeDataInSprint(boardId).then(function(assigneeMap) {
          setAssigneeName(Array.from(assigneeMap.keys()));
          const dataList = [];
          for(let columnName of columnNameList) {
            const dataObject = {};
            dataObject.label = columnName;
            dataObject.data = []
            let statusCategory = "";
            assigneeMap.forEach((statusMap, key) => {
              dataObject.data.push(statusMap.has(columnName) ? statusMap.get(columnName).count : 0);
              if(statusMap.has(columnName) && statusCategory === "") {
                statusCategory = statusMap.get(columnName).category;
              }
            });
            dataObject.backgroundColor = getColor(statusCategory);
            dataList.push(dataObject);
          }
          setDataSet(dataList);
        });
      });
    });
  }, []);

  const data = {
    labels: assigneeName,
    datasets: dataSet
  }
  const option = getOption(boardId);

  return (
    <div className="App" style={{display:'flex', alignItems:'center'}}>
      <div style={{flex:'3'}}>
        <Bar options={option} data={data} plugins={[htmlLegendPlugin]}/>
      </div>
      <div id='legend-container' style={{flex:'1'}}></div>
    </div>
  );
}

const htmlLegendPlugin = {
  id: 'htmlLegend',
  afterUpdate(chart, args, options) {
    const legendContainer = document.getElementById(options.containerID);
    let ul = legendContainer.querySelector('ul');

    if(!ul){
      ul = document.createElement("ul");
      legendContainer.appendChild(ul);
    }

    // Remove old legend items
    while (ul.firstChild) {
      ul.firstChild.remove();
    }

    //Calculate total data for each dataset
    const totalData = [];
    chart.data.datasets.forEach(dataset => {
      let total = 0;
      dataset.data.forEach(data => total += data);
      totalData.push(total);
    });

    // Reuse the built-in legendItems generator
    const items = chart.options.plugins.legend.labels.generateLabels(chart);
    let i=0;
    items.forEach(item => {
      const li = document.createElement('li');
      li.style.alignItems = 'center';
      li.style.display = 'flex';
      li.style.flexDirection = 'row';
      li.style.marginLeft = '10px';

      // Color box
      const boxSpan = document.createElement('span');
      boxSpan.style.background = item.fillStyle;
      boxSpan.style.borderColor = item.strokeStyle;
      boxSpan.style.borderWidth = item.lineWidth + 'px';
      boxSpan.style.display = 'inline-block';
      boxSpan.style.flexShrink = 0;
      boxSpan.style.height = '10px';
      boxSpan.style.marginRight = '10px';
      boxSpan.style.width = '20px';

      // Text
      const textContainer = document.createElement('p');
      textContainer.style.color = item.fontColor;
      textContainer.style.fontSize = "70%";
      textContainer.style.margin = 0;
      textContainer.style.padding = 0;
      textContainer.style.textDecoration = item.hidden ? 'line-through' : '';
      const text = document.createTextNode(item.text + " - " + totalData[i++]);
      textContainer.appendChild(text);

      li.appendChild(boxSpan);
      li.appendChild(textContainer);
      ul.appendChild(li);
    });
  }
};

const getOption = (boardId) => {
 return {
   indexAxis:'y',
   elements:{
     bar:{
       borderWidth:4,
     },
   },
   responsive:true,
   interaction:{
     mode:'index',
     intersect:false
   },
   scales:{
     x:{
       stacked:true,
       ticks: {
         stepSize: 1
       }
     },
     y:{
       stacked:true
     }
   },
   plugins:{
     htmlLegend: {
       containerID:'legend-container'
     },
     legend:{
       display:false
     },
     title:{
       display:true,
       text:'Sprint Distribution Chart for Board : ' + boardId
     }
   }
 };
}

const statusColorMap = new Map([
  ['To Do', ['beige', 'aliceblue', 'grey', 'lightgrey']],
  ['In Progress', ['slateblue', 'royalblue', 'lightblue', 'deepskyblue', 'cadetblue', 'cornflowerblue']],
  ['Done', ['olivedrab', 'darkseagreen', 'palegreen', 'seagreen', 'yellowgreen']]
]);

const getColor = (statusCategory) => {
 let colorList = statusColorMap.get(statusCategory);
 return (colorList == undefined || colorList.length == 0) ? getRandomColor() : colorList.pop();
}

const getRandomColor = () => {
  return "#" + ((1 << 24) * Math.random() | 0).toString(16).padStart(6, "0");
}

export default View;
