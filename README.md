# Sprint Distribution Chart Forge App

This project contains a Forge app written in JavaScript that displays the number of issues assigned to a user in current active sprint for given Board Id as a Bar Chart in a Jira dashboard gadget.

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge.

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

## Quick start

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

- Clone the repository:
```
git clone https://gitlab.com/jaspreet7/jiraapp
```

- Install top-level dependencies (inside of root directory):
```
npm install
```

- Install dependencies (inside of the `static/jira-gadget` directory):
```
npm install
```

- Modify your app by editing the files in `static/jira-gadget/src/`.

- Build your app (inside of the `static/jira-gadget` directory):
```
npm run build
```

- Register a new copy of this app to your developer account:
```
forge register
```

- Deploy your app by running:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

### Notes
- Use the `forge deploy` command when you want to persist code changes.
- Use the `forge install` command when you want to install the app on a new site.
- Once the app is installed on a site, the site picks up the new app changes you deploy without needing to rerun the install command.
- Use the [`forge tunnel`](https://developer.atlassian.com/platform/forge/change-the-frontend-with-forge-ui/#set-up-tunneling) command to run your Forge app locally.

## Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.

